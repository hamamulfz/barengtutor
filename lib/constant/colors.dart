import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BarengTutorColors {
  static const Color blueColor = Color(0xff23448c);
  static const Color greenColor = Color(0xff43b44b);

  static const Color colorPalleteDarkBlue = Color(0xff043353);
  static const Color colorPalleteLightBlue = Color(0xff18A4E0);
  static const Color colorPalleteLightCream = Color(0xffFAF8F0);
  static const Color colorPalleteLightGrey = Color(0xffD3DDe6);
}

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const kTextFieldDecoration = InputDecoration(
  hintText: 'Enter a value',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.blueAccent, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.blueAccent, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);

const Color titleText = Color(0xaa845EC2);
Color subtitleText = Color(0xff845EC2);

const kSendButtonTextStyle = TextStyle(
  color: titleText,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);
