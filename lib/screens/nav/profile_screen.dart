import 'package:barengtutor/constant/colors.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(Icons.more_vert),
                  ],
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  'assets/random.png',
                  width: 100,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'John Doe',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 5),
              Text(
                'Tanah Abang, Jakarta',
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
              SizedBox(height: 15),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text(
                          '12',
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Pertemuan',
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),

                  // VerticalDivider(),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text(
                          'SMA',
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Tingkatan',
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //

                  // VerticalDivider(
                  //   width: 10,
                  //   color: Colors.black,
                  // ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text(
                          '5',
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Post',
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 10,
                      ),
                      // width: double.infinity,
                      child: RaisedButton(
                        onPressed: () {},
                        color: BarengTutorColors.colorPalleteLightGrey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.edit),
                            SizedBox(width: 10),
                            Text(
                              'Edit Profile',
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 5,
                        // vertical: 10,
                      ),
                      // width: double.infinity,
                      child: RaisedButton(
                        onPressed: () {},
                        color: BarengTutorColors.blueColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          'Mentor Profile',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Pelajaran Favorit',
                          style: TextStyle(
                            // fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 20,
                        ),
                      ],
                    ),
                    Divider(),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'Matematika',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'Seni Budaya',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'B. Inggris',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Pelajaran Kelemahan',
                          style: TextStyle(
                              // fontWeight: FontWeight.bold,
                              // fontSize: 14,
                              ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 20,
                        ),
                      ],
                    ),
                    Divider(),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'Fisika',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'Kimia',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Transform.rotate(
                                  angle: 3.14,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'B. Jepang',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Divider(),
              ListTile(
                dense: true,
                visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                // contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                leading: Container(
                  decoration: BoxDecoration(
                    color: Colors.red.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: EdgeInsets.all(5),
                  child: Transform.rotate(
                    angle: 3.14,
                    child: Icon(
                      Icons.exit_to_app_outlined,
                      color: Colors.red,
                      size: 20,
                    ),
                  ),
                ),
                title: Text('Log out'),
                onTap: () {},
              ),
              SizedBox(height: 25),
            ],
          ),
        ),
      ),
    );
  }
}
