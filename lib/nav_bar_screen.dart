import 'package:barengtutor/constant/colors.dart';
import 'package:barengtutor/chat_screen.dart';
import 'package:barengtutor/screens/nav/chat_list_screen.dart';
import 'package:barengtutor/screens/nav/history_screen.dart';
import 'package:barengtutor/screens/nav/home_screen.dart';

import 'package:barengtutor/screens/nav/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  // HomeProvider _homProvider;
  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              // Navigator.of(context, rootNavigator: true).pop();
              // await Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => SecondScreen(payload),
              //   ),
              // );
            },
          )
        ],
      ),
    );
  }

  // initPusherAndNotif() async {
  //   _homProvider = Provider.of<HomeProvider>(context, listen: false);
  //   // await _homProvider.initLocalNotif(
  //   //     onDidReceiveLocalNotification: onDidReceiveLocalNotification);
  //   await _homProvider.initPusher();
  // }

  @override
  void initState() {
    super.initState();
    // initPusherAndNotif();
  }

  String barcodeScanRes;
  PageController _pc = PageController();
  // int _selectedPage = 0;
  bool isHome = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pc,
        children: [
          HomeScreen(),
          HistoryScreen(),
          ProfileScreen(),
          ChatListScreen(),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              blurRadius: 10,
              color: Colors.grey[400],
              offset: Offset(
                0,
                -2,
              ),
            )
          ],
        ),
        child: BottomAppBar(
          color: Colors.white,
          child: Container(
            height: 60,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        isHome = true;
                        setState(() {});
                        _pc.animateToPage(
                          0,
                          curve: Curves.easeInOut,
                          duration: Duration(microseconds: 1),
                        );
                      },
                      // splashColor: Colors.brown,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.home,
                            color: BarengTutorColors.blueColor,
                          ),
                          Text(
                            'Home',
                            style: TextStyle(
                                color: BarengTutorColors.blueColor,
                                fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        isHome = true;
                        setState(() {});
                        _pc.animateToPage(
                          1,
                          curve: Curves.easeInOut,
                          duration: Duration(microseconds: 1),
                        );
                      },
                      // splashColor: Colors.brown,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.history,
                            color: BarengTutorColors.blueColor,
                          ),
                          Text(
                            'History',
                            style: TextStyle(
                                color: BarengTutorColors.blueColor,
                                fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        isHome = true;
                        setState(() {});
                        _pc.animateToPage(
                          3,
                          curve: Curves.easeInOut,
                          duration: Duration(microseconds: 1),
                        );
                      },
                      // splashColor: Colors.brown,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.mail,
                            color: BarengTutorColors.blueColor,
                          ),
                          Text(
                            'Inbox',
                            style: TextStyle(
                                color: BarengTutorColors.blueColor,
                                fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        isHome = false;
                        setState(() {});
                        _pc.animateToPage(
                          2,
                          curve: Curves.easeInOut,
                          duration: Duration(microseconds: 1),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.person,
                            color: BarengTutorColors.blueColor,
                          ),
                          Text(
                            'Profile',
                            style: TextStyle(
                                color: BarengTutorColors.blueColor,
                                fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          shape: CircularNotchedRectangle(),
        ),
      ),
    );
  }
}
