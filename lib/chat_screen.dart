import 'package:barengtutor/constant/colors.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  static const String id = 'chat_screen';

  const ChatScreen({
    Key key,
  }) : super(key: key);
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();

  String messageText;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Ashley',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MessagesStream(),
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: messageTextController,
                      onChanged: (value) {
                        messageText = value;
                      },
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      // messageTextController.clear();
                      // _firestore.collection('messages').add({
                      //   'text': messageText,
                      //   'sender': loggedInUser.email,
                      // });
                    },
                    child: Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessagesStream extends StatelessWidget {
  const MessagesStream({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> chatData;
    chatData = [
      {
        "text":
            "Dear customerName, wo.workOrderType installer will be arriving at {wo.unitNo} to install {wo.package} at {wo.workDate} - {wo.workTime} . Please reconfirm your address by replying Yes or No?",
        "sender": "Modis Administrator"
      },
      // {"text": "No", "sender": "${wo.customerName}"},
      {"text": "Yes", "sender": "{wo.customerName}"},
      // {
      //   "text":
      //       "Thanks for your response. Please type the correct address so our installer will arrive at the location on time",
      //   "sender": "Modis Administrator"
      // },
      // {"text": "Jalan bazar u9/78 no5-2", "sender": "${wo.customerName}"},
      {
        "text":
            "Thanks for your response. {wo.workOrderType} installer will be arriving at the address confirmed at the planned time. Installer’s name: {wo.installerContractor.installerName}. If you have anything to chat with our installer, you can continue chatting here",
        "sender": "Modis Administrator",
      },
    ];

    if ((chatData.length == 0)) {
      return Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.lightBlueAccent,
        ),
      );
    }

    final messages = chatData.reversed;
    List<MessageBubble> messageBubbles = [];
    for (var message in messages) {
      final messageText = message['text'];
      final messageSender = message['sender'];

      final currentUser = 'Modis Administrator';

      final messageBubble = MessageBubble(
        sender: messageSender,
        text: messageText,
        isMe: currentUser == messageSender,
      );

      messageBubbles.add(messageBubble);
    }

    return Expanded(
      child: ListView(
        reverse: true,
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: messageBubbles,
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble({this.sender, this.text, this.isMe});

  final String sender;
  final String text;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            sender,
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.black54,
            ),
          ),
          Material(
            borderRadius: isMe
                ? BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0))
                : BorderRadius.only(
                    bottomLeft: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    topRight: Radius.circular(15.0),
                  ),
            elevation: 5.0,
            color: isMe ? Colors.lightBlueAccent : Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text(
                text,
                style: TextStyle(
                  color: isMe ? Colors.white : Colors.black54,
                  fontSize: 12.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
